# Network Templates

These are regular docker-compose.yml files, to run and maintain an accumulate node.
Multiple **.yml** files can be included in one template file, separated by **FILENAME=**.  Thay are split into separate files, when selected by the **run** script.

### File format
Docker yml files should start with the following
    # FILENAME=docker-compose.yml
    # DESC=DevNET - testing

Optional image list items
    # RELEASE=<image>,<image>,<image>....


And contain an initaliser
    # FILENAME=docker-compose-init.yml
